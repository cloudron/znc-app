# Cloudron ZNC

This app packages ZNC <upstream>1.6.3</upstream>

ZNC is an IRC network bounce or proxy service that remains persistently connected to your preferred IRC networks and channels.
